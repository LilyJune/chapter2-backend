/* eslint-env mocha */
// Test of authentication service
const expect = require('chai').expect
const AuthService = require('../../src/services/authentication')
const jwt = require('jsonwebtoken')

describe('authentication', () => {
  let token = null
  beforeEach((done) => {
    token = jwt.sign({ username: 'username', name: 'name', id: 'id', level: 'leve' }, 'User logged in.', { expiresIn: 28800000 })
    AuthService.saveToken(token)
    return done()
  })
  afterEach((done) => {
    AuthService.clearTokens()
    return done()
  })
  it('should successfully check a valid token that has been saved.', async (done) => {
    const badToken = jwt.sign({ username: 'bad_username', name: 'name', id: 'id', level: 'leve' }, 'User logged in.', { expiresIn: 28800000 })
    expect(AuthService.checkValidToken(token)).to.equal(true)
    expect(AuthService.checkValidToken(badToken)).to.equal(false)
    done()
  })

  it('should purge only expired tokens', async (done) => {
    try {
      const expiredToken = jwt.sign({ username: 'bad_username', name: 'name', id: 'id', level: 'leve' }, 'User logged in.', { expiresIn: -1000 })
      AuthService.saveToken(expiredToken)
      AuthService.purgeTokens()
      expect(AuthService.checkValidToken(token)).to.equal(true)
      expect(AuthService.checkValidToken(expiredToken)).to.equal(false)
      done()
    } catch (err) {
      done(err)
    }
  })
  it('should remove a token if it exists', async (done) => {
    try {
      AuthService.removeToken(token)
      expect(AuthService.checkValidToken(token)).to.equal(false)
      done()
    } catch (err) {
      done(err)
    }
  })
  it('should clear all tokens', async (done) => {
    try {
      AuthService.clearTokens()
      expect(AuthService.checkValidToken(token)).to.equal(false)
      done()
    } catch (err) {
      done(err)
    }
  })
})
