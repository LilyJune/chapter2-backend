/* eslint-env mocha */
// Test of exporting service
// const expect = require('chai').expect
// const fs = require('fs')
const request = require('request')

describe('exporter.js', function () {
  it('should create a .csv file with the databases data', (done) => {
    // test here
    done()
  })
  it('should use the cached file in tmp if no database changes have been made', (done) => {
    // test here
    done()
  })
  it('should send the .csv from the /export GET request', (done) => {
    request('http://localhost:8080/export', (err, res, body) => {
      if (err != null) {
        done(err)
      }
    })
    done()
  })
})
