/* eslint-env mocha */
// Simple example of how chai and mocha work
// Backend must be running for test to work
const expect = require('chai').expect
const request = require('request')

describe('app.js', function () {
  it('should return HelloWorld from / GET request', function (done) {
    request('http://localhost:8080', function (error, response, body) {
      if (error == null) {
        expect(body).to.equal('Hello World!')
      } else {
        console.error('ERR Occured in Req. Return With HelloWorld: ' + error)
      }
      done()
    })
  })
})
