/* eslint-env mocha */
// Test of logging service
const expect = require('chai').expect
const fs = require('fs')
const LoggingService = require('../../src/services/logging')

function getDate () {
  const date = new Date()
  return `${date.toLocaleString()} - `
}

const TEST_LOG_FILE = `./logs/test${getDate().split(',')[0].replace(/\//g, '-')}.txt`

describe('logging.js', () => {
  beforeEach((done) => {
    fs.unlink(TEST_LOG_FILE, () => {})
    return done()
  })
  afterEach((done) => {
    fs.unlink(TEST_LOG_FILE, () => {})
    return done()
  })
  it('should log the time and message to the specified log file in the standard format', (done) => {
    const message = 'This is the test message!'
    LoggingService.log(message, 'test.txt')
    fs.readFile(TEST_LOG_FILE, (err, data) => {
      if (err) {
        done(err)
      }
      const m = String(data).match(/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}, [0-9]{1,2}:[0-9]{2}:[0-9]{2} [A|P]M - (.*)\n/)[1]
      expect(m).to.equal(message)
      expect(data).to.match(/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}, [0-9]{1,2}:[0-9]{2}:[0-9]{2} [A|P]M - (.*)\n/, 'Log did not match expected format!')
      done()
    })
  })
})
