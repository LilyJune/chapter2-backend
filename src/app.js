const express = require('express')
const cors = require('cors')
const app = express()
const LoggingService = require('./services/logging')
const ExportingService = require('./services/exporter')
const DatabaseService = require('./services/database')
const AuthService = require('./services/authentication')
const ImportService = require('./services/import')
const PurgeService = require('./services/purge')
const path = require('path')

const jwt = require('jsonwebtoken')

app.use(express.json())
app.use(cors())

setInterval(PurgeService.purgeLogFiles, 43200000)

/*
 * Serves the homepage.
 * Mainly used for debugging to ensure that the backend and token authentication are working properly.
 */
app.get('/', function (req, res, next) {
  const t = jwt.decode(getToken(req))
  if (t != null) {
    res.status(200).send(`Hello World!\nUser: ${t.username}`)
  } else {
    res.status(200).send('Hello World!')
  }
  LoggingService.log('Hello World Served!')
})

/*
 * Does not serve the frontend
 * Used to ensure the backend can connect to the database
 */
app.get('/now', async function (req, res, next) {
  LoggingService.log('Serving /now')
  DatabaseService.getCurrentTime()
    .then(time => res.status(200).send(`The current time from the database is ${time}`))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves both the Import page and the Add PDF page.
 * The function will import in different ways based on the type of file imported.
 * A excel file, such as .xlsx or .xlsm, will have its data added to the Data and Instructions tables
 * A pdf file will be uploaded to the /pdf directory with a link to the file added to the Files table
 */
app.post('/import', async function (req, res, next) {
  const token = jwt.decode(await getToken(req))
  ImportService.importFile(req, token.username)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves the Export page
 * Amount of data exported depends on user permission
 * Does not export user passwords to anyone
 */
app.get('/export', async function (req, res, next) {
  PurgeService.purgeTempFiles()
  const params = await getParameters(req)
  const permission = jwt.decode(await getToken(req)).level
  LoggingService.log(`Serving /export params: ${JSON.stringify(params)} Permission: ${permission}`)
  if (params.job_number || params.work_order) {
    ExportingService.exportData(params).then(file => {
      LoggingService.log(`Sending file: ${file}`)
      res.status(200).download(path.join(__dirname, `../tmp/${file}`))
    }).catch(err => {
      LoggingService.log(err)
      res.status(500).send(err)
    })
  } else {
    ExportingService.exportAllData(permission).then(file => {
      LoggingService.log(`Sending file: ${file}`)
      res.status(200).download(path.join(__dirname, `../tmp/${file}`))
    }).catch(err => {
      LoggingService.log(err)
      res.status(500).send(err)
    })
  }
})

/*
 * Serves the Edit User page
 * Allows either adding a new user or modifying an existing user
 */
app.put('/user', async function (req, res, next) {
  LoggingService.log('Attempting to edit a user')
  LoggingService.log(JSON.stringify(req.body))

  DatabaseService.editUser(req.body)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Returns all user data except passwords
 */
app.get('/users', async function (req, res, next) {
  LoggingService.log('Endpoint for all user data called')
  DatabaseService.getUsers()
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves login page
 * Checks if username and password match a user in the database,
 * and generates a login token if the credentials are valid.
 */
app.post('/login', async function (req, res, next) {
  const { username, password } = req.body
  LoggingService.log(`User attempt to log in: ${username}`)
  DatabaseService.getUser(username, password).then(user => {
    if (user !== null && user !== undefined) {
      LoggingService.log(`User found: ${JSON.stringify(user)}`)
      const token = jwt.sign({ username: user.username, name: user.name, id: user.id, level: user.permission }, 'User logged in.', { expiresIn: 28800000 })
      AuthService.saveToken(token)
      res.status(200).json({
        success: true,
        err: null,
        token
      })
    } else {
      LoggingService.log('Invalid Username/Password!')
      res.status(401).json({
        success: false,
        err: `User ${username} does not exist or invalid password was entered!`,
        token: null
      })
    }
  }).catch(err => res.status(500).send(err))
})

/*
 * Serves the page for adding an inspection
 * Adds inspection data to a given job number and work order
 */
app.post('/form/:job_number/:work_order', async function (req, res, next) {
  DatabaseService.addJobNumberWorkOrder(req.params.job_number, req.params.work_order, req.body)
    .then(result => res.status(201).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Gets inspections for the user who is logged in, based on their permissions (whether the user is an operator, inspector, or admin)
 */
app.get('/inspections/worker', async function (req, res, next) {
  const token = jwt.decode(await getToken(req))
  if (token != null) {
    LoggingService.log(`Getting inspections for worker: ${token.username}`)
    DatabaseService.getInspectionsByWorkerType(token.level)
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  } else {
    LoggingService.log('Unauthorized user was denied inspection data')
    res.status(401).send('No authorization')
  }
})

/*
 * Serves the Inspection History page
 * Gets inspection history for a user given their ID
 */
app.get('/inspection_history/:id', async function (req, res, next) {
  DatabaseService.getUserInspectionHistory(req.params.id)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves the Edit Associations page
 * Gets job numbers associated with a work order
 */
app.get('/association/:work_order', async function (req, res, next) {
  DatabaseService.getWorkOrderJobNumberAssociations(req.params.work_order)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves the export page
 * Returns inspection data used for the tables and visuals on the export page
 * Allows for filtering by job number and work order
 */
app.get('/data', async function (req, res, next) {
  const params = await getParameters(req)
  LoggingService.log(`Serving /data params for tables and visuals: ${JSON.stringify(params)}`)
  if (params.job_number || params.work_order) {
    DatabaseService.joinData(params)
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  } else {
    DatabaseService.joinAllData()
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  }
})

/*
 * Serves page for adding instructions
 * Adds instructions to a given job number
 */
app.put('/instructions/:job_number', async function (req, res, next) {
  DatabaseService.addInstructions(req.body, req.params.job_number)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves the dashboard on the home page
 * Gets instructions for the user currently logged in
 */
app.get('/instructions/worker', async function (req, res, next) {
  const token = jwt.decode(await getToken(req))
  LoggingService.log(`Getting instructions for worker: ${token.username}`)
  const params = await getParameters(req)
  LoggingService.log(`Serving /instructions/worker: ${JSON.stringify(params)}`)
  if (params.rows || params.active_page) {
    DatabaseService.getInstructionsRangeByWorkerType(token.level, params.rows, params.active_page)
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  } else {
    DatabaseService.getInstructionsByWorkerType(token.level)
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  }
})

/*
 * Serves the dashboard on the home page
 * Gets the number of instructions for the user currently logged in,
 * so that the dashboard knows how to split them by page
 */
app.get('/instructions/count', async function (req, res, next) {
  const token = jwt.decode(await getToken(req))
  LoggingService.log(`Getting instructions for worker: ${token.username}`)
  DatabaseService.getInstructionCountByWorkerType(token.level)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Gets instructions associated with a given job number
 */
app.get('/instructions/:job_number', async function (req, res, next) {
  DatabaseService.getInstructions(req.params.job_number, null)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves /operator/form/:job_number/:work_order on frontend
 * Gets inspection data associated with a given job number and work order
 */
app.get('/form/:job_number/:work_order', async function (req, res, next) {
  DatabaseService.getInstructions(req.params.job_number, req.params.work_order)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves Add Job Number page
 * Adds a job number to the database
 */
app.post('/addJobNumber', async function (req, res, next) {
  LoggingService.log('Adding job number to database')
  DatabaseService.addJobNumber(req.body)
    .then(result => res.status(201).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves assignments dashboard on homepage
 * Add and remove one or more assignments to a given job number and inspection number combination
 * Assignments to be added are in the JSON element "assign", assignments to be removed are in "unassign"
 */
app.put('/assign', async function (req, res, next) {
  const params = await getParameters(req)
  if (params.job_number && params.inspection_number) {
    DatabaseService.updateAssignments(req.body, params.job_number, params.inspection_number)
      .then(result => res.status(200).send(result))
      .catch(err => res.status(500).send(err))
  } else {
    res.status(400).send('Must include job_number and inspection_number parameters')
  }
})

/*
 * Serves assignments dashboard on homepage
 * Returns all user assignment data
 */
app.get('/assign', async function (req, res, next) {
  LoggingService.log('Endpoint for all user assignment data called')
  DatabaseService.getAllUserAssignments()
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves to-do list on homepage
 * Gets assignment data for the user that is logged in
 */
app.get('/myAssignments', async function (req, res, next) {
  LoggingService.log('Endpoint for user assignment data called')
  const token = jwt.decode(await getToken(req))
  DatabaseService.getUserAssignments(token.id)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves Edit Associations page
 * Adds one or more associations between job numbers and work orders
 */
app.put('/associations/:work_order', async function (req, res, next) {
  DatabaseService.addJobNumberWorkOrderAssociations(req.body, req.params.work_order)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

/*
 * Serves View Advanced Instructions page
 * Gets all PDFs associated with a given job number
 * Returns an error if no PDFs are found
 */
app.get('/pdf', async function (req, res, next) {
  PurgeService.purgeTempFiles()
  const params = await getParameters(req)
  LoggingService.log(`Serving /pdf params: ${JSON.stringify(params)}`)
  ExportingService.exportPdf(params.job_number)
    .then(file => {
      LoggingService.log(`Sending file: ${file}`)
      res.status(200).download(path.join(__dirname, `../tmp/${file}`))
    })
    .catch(err => res.status(500).send(err))
})

async function getParameters (req) {
  LoggingService.log(`Getting parameters for URL: ${req.url}`)
  let params = req.url.split('?')
  if (params.length < 2) {
    return new Map()
  }
  params = params[1].split('&')
  const paramsMap = new Map()
  for (let i = 0; i < params.length; i++) {
    const split = params[i].split('=')
    if (split[1] !== 'default') {
      paramsMap[split[0]] = decodeURI(split[1])
    }
  }
  return paramsMap
}

async function getToken (req) {
  try {
    let token = req.headers['x-access-token'] || req.headers.authorization
    if (token.startsWith('Bearer ')) {
      token = token.slice(7, token.length)
    }
    return token
  } catch (err) {
    LoggingService.log('Error when getting token, no header or empty header?')
    return null
  }
}

module.exports = app
