const LoggingService = require('./logging')
const fs = require('fs')
const LOG_FILE = 'purge.txt'
const TMP_FOLDER = './tmp/'
const LOG_FOLDER = './logs/'
const MAX_AGE_MINS_TMP = 60
const MAX_AGE_MINS_LOG = 10080 // 1 week
const keep = '.gitkeep'

async function purgeTempFiles () {
  fs.readdir(TMP_FOLDER, (err, files) => {
    if (err) {
      throw err
    }
    files.forEach((element) => {
      LoggingService.logToFile(`file info: ${JSON.stringify(element)}`, LOG_FILE)
      const updatedDate = fs.statSync(TMP_FOLDER + element).mtime
      LoggingService.logToFile(`${element} modified time: ${updatedDate}`, LOG_FILE)
      if (Math.abs(Math.round(((Date.now() - updatedDate) / 1000) / 60)) > MAX_AGE_MINS_TMP && element !== keep) {
        try {
          fs.unlinkSync(TMP_FOLDER + element)
        } catch (err) {
          purgeFolderAndContents(TMP_FOLDER + element)
        }
        files.splice(files.indexOf(element), 1)
      }
    })
  })
}

async function purgeLogFiles () {
  fs.readdir(LOG_FOLDER, (err, files) => {
    if (err) {
      throw err
    }
    files.forEach((element) => {
      LoggingService.logToFile(`file info: ${JSON.stringify(element)}`, LOG_FILE)
      const updatedDate = fs.statSync(LOG_FOLDER + element).mtime
      LoggingService.logToFile(`${element} modified time: ${updatedDate}`, LOG_FILE)
      if (Math.abs(Math.round(((Date.now() - updatedDate) / 1000) / 60)) > MAX_AGE_MINS_LOG && element !== keep) {
        try {
          fs.unlinkSync(LOG_FOLDER + element)
        } catch (err) {
          fs.rmdirSync(LOG_FOLDER + element, { recursive: true })
        }
        files.splice(files.indexOf(element), 1)
      }
    })
  })
}

function purgeFile (filepath) {
  if (fs.existsSync(filepath)) { fs.unlinkSync(filepath) }
}

function purgeFolderAndContents (path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach((file, index) => {
      const curPath = `${path}/${file}`
      if (fs.lstatSync(curPath).isDirectory()) {
        purgeFolderAndContents(curPath)
      } else {
        fs.unlinkSync(curPath)
      }
    })
    fs.rmdirSync(path)
  }
};

module.exports = {
  purgeTempFiles,
  purgeLogFiles,
  purgeFile,
  purgeFolderAndContents
}
