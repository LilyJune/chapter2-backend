const LoggingService = require('./logging')
const LOG_FILE = 'import.txt'
const DatabaseService = require('./database')
const fs = require('fs')
const formidable = require('formidable')
const PurgeService = require('./purge')
const XLSX = require('xlsx')

async function importFile (req, username) {
  let e = false
  try {
    const form = new formidable.IncomingForm()
    form.parse(req, async (err, fields, files) => {
      if (err) {
        throw err
      } else if (files.file.name.split('.')[files.file.name.split('.').length - 1].includes('xl')) {
        LoggingService.log(JSON.stringify(files), LOG_FILE)
        const oldpath = files.file.path
        const newpath = `./tmp/${files.file.name}`
        PurgeService.purgeFile(newpath)
        fs.rename(oldpath, newpath, err => {
          if (err) throw err
        })
        try {
          const workbook = XLSX.readFile(newpath)
          await DatabaseService.importInstructions(workbook, username)
          await DatabaseService.importData(workbook)
        } catch (err) {
          e = err
        }
      } else if (files.file.name.split('.')[files.file.name.split('.').length - 1].includes('pdf')) {
        LoggingService.log(JSON.stringify(files), LOG_FILE)
        const oldpath = files.file.path
        const newpath = `./pdf/${fields.job_number}/${files.file.name}`
        if (!fs.existsSync(`./pdf/${fields.job_number}/`)) {
          fs.mkdirSync(`./pdf/${fields.job_number}/`)
        }
        PurgeService.purgeFile(newpath)
        fs.rename(oldpath, newpath, err => {
          if (err) throw err
        })
        try {
          await DatabaseService.importAssociateFileToAdvancedInstructions(fields.job_number, newpath)
        } catch (err) {
          e = err
        }
      } else {
        throw new Error('Invalid file type.')
      }
    })
  } catch (err) {
    throw new Error('There was an error importing the file. File may be invalid or wrong type!')
  }
  if (e !== false) {
    throw e
  }
}

module.exports = {
  importFile
}
