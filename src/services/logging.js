const LOG_FOLDER = './logs/'
const GENERAL_LOG = 'general.txt'
const fs = require('fs')

function getDate () {
  const date = new Date()
  return `${date.toLocaleString()} - `
}

function logToFile (text, filename) {
  if (typeof filename === 'undefined') {
    filename = GENERAL_LOG
  }
  const filenameSplit = filename.split('.')
  filename = `${filenameSplit.slice(0, -1).join('.')}${getDate().split(',')[0].replace(/\//g, '-')}.${filenameSplit.slice(-1)[0]}`
  fs.open(LOG_FOLDER + filename, 'a+', (err, file) => {
    if (err) throw err
    fs.appendFileSync(file, `${getDate()}${text}\n`, 'utf8', (err) => {
      fs.close(file, (err) => {
        if (err) throw err
      })
      if (err) throw err
    })
  })
}

function logToConsole (text) {
  console.log(`${getDate()}${text}`)
}

function log (text, filename) {
  module.exports.logToConsole(text)
  if (typeof filename === 'undefined') {
    filename = GENERAL_LOG
  }
  logToFile(text, filename)
}

module.exports = {
  logToFile,
  logToConsole,
  log
}
