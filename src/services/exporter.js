const LoggingService = require('./logging')
const DatabaseService = require('./database')
const PurgeService = require('./purge')
const fs = require('fs')
const archiver = require('archiver')
const LOG_FILE = 'exporter.txt'
const TMP_FOLDER = './tmp/'
const PDF_FOLDER = './pdf/'

async function exportAllData (permission) {
  LoggingService.logToFile('Exporting All Data', LOG_FILE)
  const date = new Date()
  const dir = `${date.toLocaleString().replace(/ /g, '_').replace(/\//g, '-').replace(/:/g, '-').replace(/,/g, '')}`
  fs.mkdirSync(`${TMP_FOLDER}${dir}`)
  const tables = await DatabaseService.getTables()
  if (permission < 3) {
    LoggingService.logToFile('Permission < 3, Pruning users table.', LOG_FILE)
    tables.forEach((element) => {
      LoggingService.log(`Current Element: ${element.table_name}`)
      if (element.table_name === 'users') {
        LoggingService.logToFile('Splicing Tables', LOG_FILE)
        tables.splice(tables.indexOf(element), 1)
        LoggingService.logToFile('Spliced Tables!', LOG_FILE)
      }
    })
    LoggingService.logToFile('Done Splicing', LOG_FILE)
  }
  LoggingService.logToFile(`Result of tables query: ${JSON.stringify(tables)}`, LOG_FILE)
  const tableData = []
  await asyncForEach(tables, async (element, i) => {
    tableData[i] = {}
    tableData[i].name = element.table_name
    tableData[i++].data = await DatabaseService.getTableData(element.table_name)
    LoggingService.logToFile(`Data got: ${JSON.stringify(tableData[i - 1])}`, LOG_FILE)
  })
  LoggingService.logToFile('Performed Tables Query', LOG_FILE)
  let csvdata = ''
  tableData.forEach((table) => {
    for (var propName in table.data[0]) {
      csvdata += `${escapeParse(propName)},`
    }
    csvdata = `${csvdata.substring(0, csvdata.length - 1)}\n`
    LoggingService.logToFile('Added headers to csv', LOG_FILE)
    table.data.forEach((element) => {
      for (var propName in element) {
        if (element[propName] === null) {
          csvdata += `${escapeParse('null')},`
        } else {
          csvdata += `${escapeParse(element[propName])},`
        }
      }
      csvdata = `${csvdata.substring(0, csvdata.length - 1)}\n`
    })
    LoggingService.logToFile('Added data to csv', LOG_FILE)

    const filename = `${TMP_FOLDER}${dir}/${table.name}.csv`
    fs.writeSync(fs.openSync(filename, 'w+'), csvdata, 0, csvdata.length, null)
    csvdata = ''
    LoggingService.logToFile('Wrote csv and cleared csvdata', LOG_FILE)
  })

  await zipDirectory(`${TMP_FOLDER}${dir}`)
  LoggingService.logToFile(`Wrote File: ${dir}.zip`, LOG_FILE)
  return `${dir}.zip`
}

async function exportData (params = null) {
  LoggingService.logToFile(`Exporting Data for ${JSON.stringify(params)}`, LOG_FILE)

  const data = await DatabaseService.getData(params)
  LoggingService.logToFile(`Data got: ${JSON.stringify(data)}`, LOG_FILE)

  let csvdata = 'Table: Data\n'
  for (const propName in data[0]) {
    csvdata += `${escapeParse(propName)},`
  }
  csvdata = `${csvdata.substring(0, csvdata.length - 1)}\n`
  data.forEach((element) => {
    for (const propName in element) {
      csvdata += `${escapeParse(element[propName])},`
    }
    csvdata = `${csvdata.substring(0, csvdata.length - 1)}\n`
  })

  const date = new Date()
  const filename = `${date.toLocaleString().replace(/ /g, '_').replace(/\//g, '-').replace(/:/g, '-').replace(/,/g, '')}.csv`
  const filepath = `${TMP_FOLDER}${filename}`
  fs.writeSync(fs.openSync(filepath, 'w+'), csvdata, 0, csvdata.length, null)

  LoggingService.log('Wrote File!')
  return filename
}

async function exportPdf (jobNumber) {
  const filePaths = await DatabaseService.getPdfPaths(jobNumber)
  if (filePaths.length <= 0) {
    throw new Error('No Files Found For That Job Number!')
  }
  PurgeService.purgeFolderAndContents(`${TMP_FOLDER}/${jobNumber}`)
  fs.mkdirSync(`${TMP_FOLDER}${jobNumber}`)
  filePaths.forEach((path) => {
    const filename = path.file_path.split('/').pop()
    const src = `${PDF_FOLDER}/${jobNumber}/${filename}`
    const dest = `${TMP_FOLDER}${jobNumber}/${filename}`
    LoggingService.logToFile(`Copying from ${src} to ${dest}`)
    fs.copyFileSync(src, dest)
  })
  await zipDirectory(`${TMP_FOLDER}${jobNumber}`)
  LoggingService.logToFile(`Wrote File: ${jobNumber}.zip`, LOG_FILE)
  return `${jobNumber}.zip`
}

async function asyncForEach (array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index)
  }
}

async function zipDirectory (dir) {
  const archive = archiver('zip', { zlib: { level: 9 } })
  const stream = fs.createWriteStream(`${dir}.zip`)
  await archive.directory(dir, false).pipe(stream)
  await archive.finalize()
  await waitForStream(stream)
  await stream.close()
}

async function waitForStream (stream) {
  return new Promise((resolve) => {
    stream.on('close', () => {
      LoggingService.logToFile('Archiver stream closed!', LOG_FILE)
      resolve()
    })
  })
}

function escapeParse (input) {
  let i = input.length
  while (i--) {
    if (input.charAt(i) === '"') {
      if (i !== input.length - 1) {
        input = `${input.substring(0, i)}"${input.substring(i + 1)}`
      } else {
        input = `${input.substring(0, i + 1)}"`
      }
    }
  }
  return `"${input}"`
}

module.exports = {
  exportAllData,
  exportData,
  exportPdf
}
