const LoggingService = require('./logging')
const LOG_FILE = 'authentication.txt'
const jwt = require('jsonwebtoken')

let tokens = []

function saveToken (token) {
  LoggingService.logToFile(`Saving token: ${JSON.stringify(token)}`, LOG_FILE)
  tokens.push(token)
}

function checkValidToken (token) {
  LoggingService.logToFile(`Checking token: ${JSON.stringify(token)}`, LOG_FILE)
  purgeTokens()
  for (let i = 0; i < tokens.length; i++) {
    if (token === tokens[i]) {
      LoggingService.logToFile('Token Matched', LOG_FILE)
      return true
    }
  }
  LoggingService.logToFile('Token Invalid', LOG_FILE)
  return false
}

function removeToken (token) {
  LoggingService.logToFile(`Removing Token: ${JSON.stringify(token)}`, LOG_FILE)
  for (let i = 0; i < tokens.length; i++) {
    if (token === tokens[i]) {
      tokens.splice(i, 1)
      LoggingService.logToFile('Token Removed', LOG_FILE)
    }
  }
  LoggingService.logToFile('Tried Removing Token That Does Not Exist', LOG_FILE)
}

function clearTokens () {
  LoggingService.logToFile('Clearing All Tokens', LOG_FILE)
  tokens = []
}

function purgeTokens () {
  LoggingService.logToFile('Purging Expired Tokens', LOG_FILE)
  for (let i = 0; i < tokens.length; i++) {
    const exp = jwt.decode(tokens[i]).exp
    if (exp < Date.now() / 1000) {
      tokens.splice(i, 1)
      LoggingService.logToFile('Expired Token Removed', LOG_FILE)
    }
  }
}

module.exports = {
  saveToken,
  checkValidToken,
  removeToken,
  clearTokens,
  purgeTokens
}
