const LoggingService = require('./logging')
const pg = require('pg')
const LOG_FILE = 'database.txt'
const map = require('lodash.map')

const config = {
  user: process.env.PSQL_USER,
  database: process.env.PSQL_DB,
  password: process.env.PSQL_PASS,
  port: process.env.PSQL_PORT,
  max: 10 // max number of connection can be open to database
}
const pool = new pg.Pool(config)

// Does not call any tables

function getCurrentTime () {
  return new Promise((resolve, reject) => {
    pool.query('SELECT NOW()', (error, result) => {
      if (error) {
        LoggingService.log(`Error getting time from database! ${error.stack}`)
        reject(error)
      } else {
        const time = result.rows[0].now
        resolve(time)
      }
    })
  })
}

// Can call any table

function importData (workbook) {
  /* workbook should be a json workbook created by using xlsx to parse an Excel file
  From 2nd tab and 10th tab: DataHistory and Data, respectively

  Insert into data:
  date from a combination of Date (A) and Time (B) columns
  operator from Name column (C)
  shift from Shift (D)
  inspection_number from Inspection Number (E)
  characteristic from Characteristic (F)
  result from Result (G)
  remark from Remarks (H)
  work_order from Work Order (I)
  job_number from the 7th tab: Header
  inspection_type from looking up inspection number in instructions
  machine from 7th tab: Header
  */

  LoggingService.logToFile('Importing workbook into database', LOG_FILE)
  return new Promise((resolve, reject) => {
    const jobNumber = workbook.Sheets.Header.B1.v
    const machine = workbook.Sheets.Header.B3.v
    let queryText = `INSERT INTO ${config.database}.public.data (date, operator, shift, inspection_number, characteristic, result, remark, work_order, job_number, inspection_type, machine) VALUES `
    const vals = []
    let row = 2
    let i = 0
    while (workbook.Sheets.DataHistory[`A${row}`]) {
      queryText += `($${i * 11 + 1}, $${i * 11 + 2}, $${i * 11 + 3}, $${i * 11 + 4}, $${i * 11 + 5}, $${i * 11 + 6}, $${i * 11 + 7}, $${i * 11 + 8}, $${i * 11 + 9}, $${i * 11 + 10}, $${i * 11 + 11}), `
      const date = workbook.Sheets.DataHistory[`A${row}`].v && workbook.Sheets.DataHistory[`B${row}`] ? parseDateTime(workbook.Sheets.DataHistory[`A${row}`].w, workbook.Sheets.DataHistory[`B${row}`].w) : 'NOW()'
      let operator = workbook.Sheets.DataHistory[`C${row}`] ? workbook.Sheets.DataHistory[`C${row}`].v : ''
      operator = operator.toUpperCase()
      const shift = workbook.Sheets.DataHistory[`D${row}`] ? workbook.Sheets.DataHistory[`D${row}`].v : ''
      const inspectionNumber = workbook.Sheets.DataHistory[`E${row}`] ? workbook.Sheets.DataHistory[`E${row}`].v : ''
      const characteristic = workbook.Sheets.DataHistory[`F${row}`] ? workbook.Sheets.DataHistory[`F${row}`].v : ''
      const result = workbook.Sheets.DataHistory[`G${row}`] ? workbook.Sheets.DataHistory[`G${row}`].v : ''
      const remark = workbook.Sheets.DataHistory[`H${row}`] ? workbook.Sheets.DataHistory[`H${row}`].v : ''
      const workOrder = workbook.Sheets.DataHistory[`I${row}`] ? workbook.Sheets.DataHistory[`I${row}`].v : ''
      const inspectionType = getInspectionType(inspectionNumber, jobNumber)
      vals.push(date, operator, shift, inspectionNumber, characteristic, result, remark, workOrder, jobNumber, inspectionType, machine)
      row += 1
      i += 1
    }
    queryText = queryText.substring(0, queryText.length - 2)
    pool.query(queryText, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error importing workbook into database! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

function importInstructions (workbook, username) {
  /* workbook should be a json workbook created by using xlsx to parse an Excel file
  From 4th tab and 5th tab: Operator and Inspector, respectively:

  Insert into instructions:
  inspection_number from Inspection Number column (A)
  name from Characteristic to Inspect (B)
  method from Method of Inspection (C)
  frequency from Frequency of Inspection (D)
  ??? from Chart 1 Part Every (E, but I guess that data goes unused for now)
  expected/tolerance calculated from Min (F)
  expected/tolerance calculated from Max (G)
  job_number from 7th tab: Header (B1)
  inspection_type from tab name
  setup_tech from the login data
  */

  LoggingService.logToFile('Importing workbook into database', LOG_FILE)
  return new Promise((resolve, reject) => {
    const jobNumber = workbook.Sheets.Header.B1.v
    let queryText = `INSERT INTO ${config.database}.public.instructions (inspection_number, frequency, setup_tech, method, expected, name, form_type, job_number, inspection_type, tolerance) VALUES `
    const vals = []
    let row = 2
    let i = 0
    while (workbook.Sheets.Operator[`A${row}`]) {
      queryText += `($${i * 10 + 1}, $${i * 10 + 2}, $${i * 10 + 3}, $${i * 10 + 4}, $${i * 10 + 5}, $${i * 10 + 6}, $${i * 10 + 7}, $${i * 10 + 8}, $${i * 10 + 9}, $${i * 10 + 10}), `
      let inspectionNumber = parseInt(workbook.Sheets.Operator[`A${row}`].v)
      if (isNaN(inspectionNumber)) {
        inspectionNumber = -1
      }
      const frequency = workbook.Sheets.Operator[`D${row}`] ? workbook.Sheets.Operator[`D${row}`].v : ''
      const method = workbook.Sheets.Operator[`C${row}`] ? workbook.Sheets.Operator[`C${row}`].v : ''
      const expected = workbook.Sheets.Operator[`F${row}`] && workbook.Sheets.Operator[`G${row}`] ? (workbook.Sheets.Operator[`G${row}`].v + workbook.Sheets.Operator[`F${row}`].v) / 2 : ''
      const name = workbook.Sheets.Operator[`B${row}`] ? workbook.Sheets.Operator[`B${row}`].v : ''
      const tolerance = workbook.Sheets.Operator[`F${row}`] && workbook.Sheets.Operator[`G${row}`] ? (workbook.Sheets.Operator[`G${row}`].v - workbook.Sheets.Operator[`F${row}`].v) / 2 : ''
      let formType = 'Input'
      if (expected === 1 && tolerance === 0) {
        formType = 'Checkbox'
      }
      vals.push(inspectionNumber, frequency, username, method, expected, name, formType, jobNumber, 'Operator', tolerance)
      row += 1
      i += 1
    }
    row = 2
    while (workbook.Sheets.Inspector[`A${row}`]) {
      queryText += `($${i * 10 + 1}, $${i * 10 + 2}, $${i * 10 + 3}, $${i * 10 + 4}, $${i * 10 + 5}, $${i * 10 + 6}, $${i * 10 + 7}, $${i * 10 + 8}, $${i * 10 + 9}, $${i * 10 + 10}), `
      let inspectionNumber = parseInt(workbook.Sheets.Inspector[`A${row}`].v)
      if (isNaN(inspectionNumber)) {
        inspectionNumber = -1
      }
      const frequency = workbook.Sheets.Inspector[`D${row}`] ? workbook.Sheets.Inspector[`D${row}`].v : ''
      const method = workbook.Sheets.Inspector[`C${row}`] ? workbook.Sheets.Inspector[`C${row}`].v : ''
      const expected = workbook.Sheets.Inspector[`F${row}`] && workbook.Sheets.Inspector[`G${row}`] ? (workbook.Sheets.Inspector[`G${row}`].v + workbook.Sheets.Inspector[`F${row}`].v) / 2 : ''
      const name = workbook.Sheets.Inspector[`B${row}`] ? workbook.Sheets.Inspector[`B${row}`].v : ''
      const tolerance = workbook.Sheets.Inspector[`F${row}`] && workbook.Sheets.Inspector[`G${row}`] ? (workbook.Sheets.Inspector[`G${row}`].v - workbook.Sheets.Inspector[`F${row}`].v) / 2 : ''
      let formType = 'Input'
      if (expected === 1 && tolerance === 0) {
        formType = 'Checkbox'
      }
      vals.push(inspectionNumber, frequency, username, method, expected, name, formType, jobNumber, 'Inspector', tolerance)
      row += 1
      i += 1
    }
    queryText = queryText.substring(0, queryText.length - 2)
    pool.query(queryText, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error importing workbook into database! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

function getTables () {
  LoggingService.log('getTables called.')
  return new Promise((resolve, reject) => {
    pool.query(`SELECT table_name FROM ${config.database}.information_schema.tables WHERE table_type='BASE TABLE' AND table_schema='public'`, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting tables from database! ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

function getTableData (tableName) {
  LoggingService.logToFile(`getTableData called. Table: ${tableName}`, LOG_FILE)
  return new Promise((resolve, reject) => {
    if (tableName !== 'users') {
      pool.query(`SELECT * FROM ${config.database}.public.${tableName}`, (error, result) => {
        if (error) {
          LoggingService.log(`Error getting tables from database! ${error.stack}`)
          reject(error)
        } else {
          LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
          resolve(result.rows)
        }
      })
    } else {
      pool.query(`SELECT permission, id.toUpperCase(), name, username FROM ${config.database}.public.${tableName}`, (error, result) => {
        if (error) {
          LoggingService.log(`Error getting tables from database! ${error.stack}`)
          reject(error)
        } else {
          LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
          resolve(result.rows)
        }
      })
    }
  })
}

// Users table

async function editUser (body) {
  LoggingService.logToFile('editUser called', LOG_FILE)
  return new Promise((resolve, reject) => {
    pool.connect((error, client, release) => {
      if (error) {
        LoggingService.log(`Error inserting user into database ${error.stack}`, LOG_FILE)
        reject(error)
      } else {
        LoggingService.logToFile(`CREATE acquiring client ${(body.function)}`, LOG_FILE)
        if (body.function !== undefined) {
          var queryText = `INSERT INTO ${config.database}.public.users (id, name, username, password, permission) VALUES ($1, $2, $3, $4, $5)`
          var vals = [body.id.toUpperCase(), body.name, body.username, body.password, body.permission]
          client.query(queryText, vals, (error, result) => {
            if (error) {
              LoggingService.log(`Error inserting user into database ${error.stack}`, LOG_FILE)
              reject(error)
            } else {
              resolve(result.rows)
            }
          })
        } else {
          var updateQueryText = ''
          if (body.permission !== undefined && body.password !== undefined) {
            updateQueryText = `UPDATE ${config.database}.public.users SET permission = $2, password = $3 WHERE username = $1`
            vals = [body.username, body.permission, body.password]
          } else if (body.password !== undefined) {
            updateQueryText = `UPDATE ${config.database}.public.users SET password = $2 WHERE username = $1`
            vals = [body.username, body.password]
          } else if (body.permission !== undefined) {
            updateQueryText = `UPDATE ${config.database}.public.users SET permission = $1 WHERE username = $2`
            vals = [body.permission, body.username]
          }
          client.query(updateQueryText, vals, (error, result) => {
            if (error) {
              LoggingService.log(`Error inserting user into database ${error.stack}`, LOG_FILE)
              reject(error)
            } else {
              resolve(result.rows)
            }
          })
        }
      }
    })
  })
}

function getUsers () {
  LoggingService.logToFile('getUsers called', LOG_FILE)
  return new Promise((resolve, reject) => {
    pool.query(`SELECT name, username, id, permission FROM ${config.database}.public.users`, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting user data from database! ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

function getUser (username, password) {
  LoggingService.logToFile(`getUser called for user: ${username}`, LOG_FILE)
  const query = `SELECT username, name, id, permission FROM ${config.database}.public.users WHERE username = $1 AND password = $2`
  const values = [username, password]
  return new Promise((resolve, reject) => {
    pool.query(query, values, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting user: ${username} : ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result.rows)}`, LOG_FILE)
        resolve(result.rows[0])
      }
    })
  })
}

// Data (inspections) table

async function addJobNumberWorkOrder (jobNumber, workOrder, body) {
  return new Promise((resolve, reject) => {
    const text = `INSERT INTO ${config.database}.public.data (operator, machine, remark, work_order, characteristic, result, date, shift, job_number, inspection_type, inspection_number) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`
    const vals = [body.operator.toUpperCase(), body.machine, body.remark, workOrder, body.characteristic, body.result, 'NOW()', body.shift, jobNumber, body.inspection_type, body.inspection_number]
    pool.query(text, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error inserting job number / work order combo into database ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

async function getData (params = null) {
  LoggingService.log(`Getting data for jobNumber: ${JSON.stringify(params)}`, LOG_FILE)
  return new Promise((resolve, reject) => {
    let query = `SELECT * FROM ${config.database}.public.data`
    const values = []
    if (params !== null) {
      query += ' WHERE '
      let i = 1
      for (const key in params) {
        if (params[key] !== undefined) {
          query += `${key}=$${i++} and `
          values.push(params[key])
        }
      }
      query = query.substring(0, query.length - 4)

      pool.query(query, values, (error, result) => {
        if (error) {
          LoggingService.log(`Error getting data with ${JSON.stringify(params)} : ${error.stack}`)
          reject(error)
        } else {
          resolve(result.rows)
        }
      })
    }
  })
}

function getInspectionsByWorkerType (type) {
  LoggingService.logToFile(`getting inspections by worker type ${type}`, LOG_FILE)
  return new Promise((resolve, reject) => {
    let query = ''
    if (type < 1) {
      resolve([])
    } else if (type === 1) {
      type = 'Operator'
      query = `SELECT job_number, work_order, inspection_type, date FROM ${config.database}.public.data WHERE inspection_type = 'Operator'`
    } else {
      query = `SELECT job_number, work_order, inspection_type, date FROM ${config.database}.public.data WHERE inspection_type = 'Operator' OR inspection_type = 'Inspector'`
    }
    pool.query(query, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting instructions by worker type: ${type}`)
        reject(error)
      } else {
        const values = {
          type: `${type}`,
          data: result.rows
        }
        resolve(values)
      }
    })
  })
}

function getUserInspectionHistory (body) {
  LoggingService.logToFile(`getting inspections history of user with id ${body.id}`, LOG_FILE)
  return new Promise((resolve, reject) => {
    const query = `SELECT * FROM ${config.database}.public.data WHERE operator=$1`
    const vals = [body.toUpperCase()]
    pool.query(query, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting user data from database! ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

async function getWorkOrderJobNumberAssociations (workOrder) {
  return new Promise((resolve, reject) => {
    const query = `SELECT job_number FROM ${config.database}.public.data WHERE work_order=$1`
    pool.query(query, [workOrder], (error, result) => {
      if (error) {
        LoggingService.log(`Error getting associations for work_order:${workOrder} from database table: data ${error.stack}`)
        reject(error)
      } else {
        const jobs = map(result.rows, 'job_number')
        const values = {
          work_order: `${workOrder}`,
          job_numbers: jobs

        }
        resolve(values)
      }
    })
  })
}

// Join data and instructions tables

function joinData (params) {
  LoggingService.logToFile('Joining All Table Data', LOG_FILE)
  return new Promise((resolve, reject) => {
    let query = `SELECT * FROM ${config.database}.public.data 
               FULL JOIN ${config.database}.public.instructions 
               ON data.inspection_number =
                  instructions.inspection_number
               AND data.job_number = 
                  instructions.job_number `
    const vals = []
    if (typeof (params.work_order) !== 'undefined') {
      if (params.job_number === 'default') {
        query += 'WHERE data.work_order = $1'
        vals.push(params.work_order)
      } else {
        query += 'WHERE (data.job_number = $1 OR instructions.job_number = $1) AND data.work_order = $2'
        vals.push(params.job_number, params.work_order)
      }
    } else {
      if (params.job_number === 'default') {
        reject(new Error('No work order was given'))
      } else {
        query += 'WHERE data.job_number = $1 OR instructions.job_number = $1'
        vals.push(params.job_number)
      }
    }
    pool.query(query, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting tables from database! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

function joinAllData () {
  LoggingService.logToFile('Joining All Table Data', LOG_FILE)
  return new Promise((resolve, reject) => {
    pool.query(`SELECT * FROM ${config.database}.public.data 
                    FULL JOIN ${config.database}.public.instructions 
                    ON data.inspection_number =
                       instructions.inspection_number
                    AND data.job_number = 
                        instructions.job_number`,
    (error, result) => {
      if (error) {
        LoggingService.log(`Error getting tables from database! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

// Instructions table

async function addInstructions (body, jobNumber) {
  return new Promise((resolve, reject) => {
    pool.connect((error, client, release) => {
      if (error) {
        LoggingService.log(`Error acquiring client ${error.stack}`)
        reject(error)
      } else {
        const deleteText = `DELETE FROM ${config.database}.public.instructions WHERE job_number=$1`
        let queryText = `INSERT INTO ${config.database}.public.instructions (inspection_number, frequency, setup_tech, method, expected, name, form_type, job_number, tolerance, inspection_type) VALUES`
        const vals = []
        for (let i = 0; i < body.characteristics.length; i++) {
          queryText += `($${i * 10 + 1}, $${i * 10 + 2}, $${i * 10 + 3}, $${i * 10 + 4}, $${i * 10 + 5}, $${i * 10 + 6}, $${i * 10 + 7}, $${i * 10 + 8}, $${i * 10 + 9}, $${i * 10 + 10}), `
          vals.push(body.characteristics[i].inspection_number, body.characteristics[i].frequency, body.characteristics[i].setup_tech, body.characteristics[i].method, body.characteristics[i].expected, body.characteristics[i].name, body.characteristics[i].form_type, jobNumber, body.characteristics[i].tolerance, body.characteristics[i].inspection_type)
        }
        queryText = queryText.substring(0, queryText.length - 2)
        client.query(deleteText, [jobNumber], (error, result) => {
          if (error) {
            LoggingService.log(`Error removing instructions from database ${error.stack}`)
            reject(error)
          } else {
            client.query(queryText, vals, (error, result) => {
              release()
              if (error) {
                LoggingService.log(`Error inserting instructions into database ${error.stack}`)
                reject(error)
              } else {
                resolve(result.rows)
              }
            })
          }
        })
      }
    })
  })
}

async function getInstructions (jobNumber, workOrder = null) {
  return new Promise((resolve, reject) => {
    const query = `SELECT * FROM ${config.database}.public.instructions WHERE job_number=$1`
    pool.query(query, [jobNumber], (error, result) => {
      if (error) {
        LoggingService.log(`Error getting instructions with job_number: ${jobNumber}, work_order:${workOrder} from database table: instructions${error.stack}`)
        reject(error)
      } else {
        let values
        if (workOrder === null) {
          values = {
            job_number: `${jobNumber}`,
            characteristics: result.rows
          }
        } else {
          values = {
            job_number: `${jobNumber}`,
            work_order: `${workOrder}`,
            characteristics: result.rows
          }
        }
        resolve(values)
      }
    })
  })
}

async function getInstructionsByWorkerType (type) {
  return new Promise((resolve, reject) => {
    let query = ''
    let data = []
    if (type < 1) {
      resolve([])
    } else if (type === 1) {
      type = 'Operator'

      query = `SELECT job_number, inspection_number, frequency, inspection_type,
      form_type, setup_tech, method, expected, tolerance
      FROM ${config.database}.public.instructions
      WHERE inspection_type=$1`
      data = [type]
    } else {
      query = `SELECT job_number, inspection_number, frequency, inspection_type,
      form_type, setup_tech, method, expected, tolerance
      FROM ${config.database}.public.instructions`
    }
    pool.query(query, data, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting instructions by worker type: ${type}`)
        reject(error)
      } else {
        const values = {
          type: `${type}`,
          data: result.rows
        }
        resolve(values)
      }
    })
  })
}

async function getInstructionsRangeByWorkerType (type, rows, activePage) {
  return new Promise((resolve, reject) => {
    let query = ''
    let data = []
    const offset = (activePage * rows) - rows
    if (type < 1) {
      resolve([])
    } else if (type === 1) {
      type = 'Operator'

      query = `SELECT job_number, inspection_number, frequency, inspection_type,
      form_type, setup_tech, method, expected, tolerance
      FROM ${config.database}.public.instructions
      WHERE inspection_type=$1
      LIMIT ${rows} OFFSET ${offset}`
      data = [type]
    } else {
      query = `SELECT job_number, inspection_number, frequency, inspection_type,
      form_type, setup_tech, method, expected, tolerance
      FROM instructions
      LIMIT ${rows} OFFSET ${offset}`
    }
    pool.query(query, data, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting instructions by worker type: ${type}`)
        reject(error)
      } else {
        const values = {
          type: `${type}`,
          data: result.rows
        }
        resolve(values)
      }
    })
  })
}

async function getInstructionCountByWorkerType (type) {
  return new Promise((resolve, reject) => {
    let query = ''
    let data = []
    if (type < 1) {
      resolve([])
    } else if (type === 1) {
      type = 'Operator'

      query = `SELECT COUNT(*)
      FROM ${config.database}.public.instructions
      WHERE inspection_type=$1`
      data = [type]
    } else {
      query = `SELECT COUNT(*)
      FROM ${config.database}.public.instructions`
    }
    pool.query(query, data, (error, result) => {
      if (error) {
        LoggingService.log(`Error getting instruction count by worker type: ${type}`)
        reject(error)
      } else {
        const values = {
          count: result.rows[0].count
        }
        resolve(values)
      }
    })
  })
}

function getInspectionType (inspectionNumber, jobNumber) {
  return new Promise((resolve, reject) => {
    pool.query(`SELECT inspection_type FROM ${config.database}.public.instructions WHERE inspection_number = $1 and job_number = $2`, [inspectionNumber, jobNumber], (error, result) => {
      if (error) {
        LoggingService.log(`Error getting inspection type! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

// Jobs table

async function addJobNumber (body) {
  return new Promise((resolve, reject) => {
    LoggingService.log(JSON.stringify(body), LOG_FILE)
    const text = `INSERT INTO ${config.database}.public.jobs (advanced_instructions, job_number, part_number, part_name, customer, operation) VALUES($1, $2, $3, $4, $5, $6)`
    const vals = [body.advanced_instructions, body.job_number, body.part_number, body.part_name, body.customer, body.operator]
    pool.query(text, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error creating job number in the database ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

// Assignments table

function updateAssignments (body, jobNumber, inspectionNumber) {
  return new Promise((resolve, reject) => {
    pool.connect(async (error, client, release) => {
      if (error) {
        LoggingService.log(`Error acquiring client ${error.stack}`)
        reject(error)
      } else {
        let assignQuery = `INSERT INTO ${config.database}.public.assignments (job_number, inspection_number, user_id) VALUES`
        const assignVals = []
        for (let i = 0; i < body.assign.length; i++) {
          assignQuery += `($${i * 3 + 1}, $${i * 3 + 2}, $${i * 3 + 3}), `
          assignVals.push(jobNumber, inspectionNumber, body.assign[i])
        }
        assignQuery = assignQuery.substring(0, assignQuery.length - 2)

        let unassignQuery = `DELETE FROM ${config.database}.public.assignments WHERE job_number=$1 AND inspection_number=$2 AND (`
        const unassignVals = [jobNumber.toString(), inspectionNumber.toString()]
        for (let i = 0; i < body.unassign.length; i++) {
          unassignQuery += `user_id=$${i + 3} OR `
          unassignVals.push(body.unassign[i].toString())
        }
        unassignQuery = unassignQuery.substring(0, unassignQuery.length - 4)
        unassignQuery += ')'
        try {
          if (body.assign.length) {
            await client.query(assignQuery, assignVals)
          }
          if (body.unassign.length) {
            await client.query(unassignQuery, unassignVals)
          }
          release()
          resolve({})
        } catch (error) {
          LoggingService.log(`Error modifying user assignments! ${error.stack}`)
          reject(error)
        }
      }
    })
  })
}

function getAllUserAssignments () {
  LoggingService.logToFile('getAllUserAssignments called', LOG_FILE)
  return new Promise((resolve, reject) => {
    pool.connect(async (error, client, release) => {
      if (error) {
        LoggingService.log(`Error acquiring client! ${error.stack}`)
        reject(error)
      } else {
        const assignments = {}
        try {
          const userResult = await client.query(`SELECT id AS user_id, name AS user_name, username AS user_username FROM ${config.database}.public.users`)
          assignments.users = userResult.rows
          const assignResult = await client.query(
            `SELECT job_number, inspection_number, ARRAY_AGG(DISTINCT user_id) assigned FROM ${config.database}.public.assignments GROUP BY job_number, inspection_number`)
          assignments.inspection_numbers = assignResult.rows
          release()
          resolve(assignments)
        } catch (error) {
          LoggingService.log(`Error getting user assignment data from database! ${error.stack}`)
          reject(error)
        }
      }
    })
  })
}

function getUserAssignments (id) {
  LoggingService.logToFile('getUserAssignments called', LOG_FILE)
  return new Promise((resolve, reject) => {
    pool.query(`SELECT * FROM ${config.database}.public.assignments WHERE user_id = $1`, [id], (error, result) => {
      if (error) {
        LoggingService.log(`Error getting assignments for the current user! ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

// Associations table

async function addJobNumberWorkOrderAssociations (body, workOrder) {
  return new Promise((resolve, reject) => {
    let queryText = `INSERT INTO ${config.database}.public.associations (job_number, work_order) VALUES`
    const vals = []
    for (let i = 0; i < body.job_numbers.length; i++) {
      queryText += `($${i * 2 + 1}, $${i * 2 + 2}), `
      vals.push(body.job_numbers[i], workOrder)
    }
    queryText = queryText.substring(0, queryText.length - 2)
    pool.query(queryText, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error inserting associations with work order ${workOrder} into database ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

// Files

async function importAssociateFileToAdvancedInstructions (jobNumber, filePath) {
  return new Promise((resolve, reject) => {
    LoggingService.log(JSON.stringify(filePath), LOG_FILE)
    const text = `INSERT INTO ${config.database}.public.files (job_number, file_path) VALUES($1, $2)`
    const vals = [jobNumber, filePath]
    pool.query(text, vals, (error, result) => {
      if (error) {
        LoggingService.log(`Error associating pdf to advanced instructions ${error.stack}`)
        reject(error)
      } else {
        resolve(result.rows)
      }
    })
  })
}

function getPdfPaths (jobNumber) {
  return new Promise((resolve, reject) => {
    pool.query(`SELECT file_path FROM ${config.database}.public.files WHERE job_number = $1`, [jobNumber], (error, result) => {
      if (error) {
        LoggingService.log(`Error getting pdf! ${error.stack}`)
        reject(error)
      } else {
        LoggingService.logToFile(`Result: ${JSON.stringify(result)}`, LOG_FILE)
        resolve(result.rows)
      }
    })
  })
}

// Helper functions

function parseDateTime (date, time) {
  const splitDate = date.split('/')
  const year = splitDate[2]
  const month = splitDate[0]
  const day = splitDate[1]
  const splitTime = time.split(' ')
  const splitSplitTime = splitTime[0].split(':')
  let hour = parseInt(splitSplitTime[0])
  const minute = splitSplitTime[1]
  if (splitTime[1] === 'PM' && hour < 12) {
    hour += 12
  } else if (splitTime[1] === 'AM' && hour === 12) {
    hour = 0
  }
  return `20${year}-${month}-${day} ${hour}:${minute}:00`
}

// Should have all the functions in the same order as above

module.exports = {
  getCurrentTime,
  importData,
  importInstructions,
  getTables,
  getTableData,
  editUser,
  getUsers,
  getUser,
  addJobNumberWorkOrder,
  getData,
  getInspectionsByWorkerType,
  getUserInspectionHistory,
  getWorkOrderJobNumberAssociations,
  joinData,
  joinAllData,
  addInstructions,
  getInstructions,
  getInstructionsByWorkerType,
  getInstructionsRangeByWorkerType,
  getInstructionCountByWorkerType,
  addJobNumber,
  updateAssignments,
  getAllUserAssignments,
  getUserAssignments,
  addJobNumberWorkOrderAssociations,
  importAssociateFileToAdvancedInstructions,
  getPdfPaths
}
