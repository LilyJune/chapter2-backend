# Backend

## Dotenv

Before starting, please create a file `.env` in the backend folder.

Please specify these values based on the setup of your database:

```
PSQL_USER=''
PSQL_PASS=''
PSQL_DB=''
PSQL_PORT=
```

Using default values provided by docker, the `.env` file should look like:

```
PSQL_USER='postgres'
PSQL_PASS='changeme'
PSQL_DB='postgres'
PSQL_PORT=5432
```


## Backend Development Environment Guide

### Tools

* Git
* NodeJS
* NPM
* Docker
* PostgreSQL
* Gitlab-CI

### Installing the Tools

#### Ubuntu (recommended due to better Docker compatibility)

[Download the latest version](https://ubuntu.com/download/desktop)


##### Git

On ubuntu you can do this all through a terminal:

```
sudo apt update
sudo apt install git
```

After installation it can be checked in the command line with:

```
git -v
```


##### NodeJS & NPM

On ubuntu extensive instructions can be found [here](https://github.com/nodesource/distributions/blob/master/README.md#snap).

For a simplified instructions, we are going to install the snap package manager in order to access this package.

```
sudo apt update
sudo apt install snapd
```

This may take a bit.

After this is done you will be able to install node and npm:

```
sudo snap install node --classic --channel=12
```

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```

##### Node Packages

Open the backend folder. In a terminal, this can be done by being in the folder above and entering `cd ./backend/`.

To install everything else you will need to run the command `npm install`.

##### Docker and PostgreSQL

Run `sudo apt-get install docker` and `sudo apt-get install docker-compose` to install docker and docker-compose.

From your project directory, navigate to the development environment using `cd docker_compose/development`.

With Docker running, run the command `docker-compose up`. This will both install PostgreSQL and start it running.

Some additional useful Docker commands can be found at the bottom of this file.


##### Starting the project

Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start` or use `npm run dev` to start the dev environment (automatically reloads the project when source code is changed).



#### Windows

##### Git

Git is the tool used for version control for this project. Git can be installed on windows by simply downloading and running the executable from [here](https://git-scm.com/download/win).

After installation it can be checked in the command line with:

```
git -v
```

##### NodeJS & NPM

NodeJS and NPM come with each other. On Windows this is a simple install. Simply download and run the stable version installer of NodeJS from [here](https://nodejs.org/en/). The stable one always has an even number before the first dot.

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```

##### Node Packages

Open the backend folder. In a terminal, this can be done by being in the folder above and entering `cd ./backend/`.

To install everything else you will need to run the command `npm install`.

##### Docker and PostgreSQL

Download and install [Docker Desktop](https://docs.docker.com/docker-for-windows/install/) for Windows.

From your project directory, navigate to the development environment using `cd docker_compose/development`.

With Docker running, run the command `docker-compose up`. This will both install PostgreSQL and start it running.

Some additional useful Docker commands can be found at the bottom of this file.


##### Starting the project

Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start` or use `npm run dev` to start the dev environment (automatically reloads the project when source code is changed).


## Available Scripts

Unless otherwise noted, these scripts can be run in the project directory:

## NPM tools
* Normal Start: `npm start`
* Developer Start (automatically reloads the project when changes are made): `npm run dev`
* Launch the test runner: `npm test`
* Standardize code: `npm run standardize`


## Useful tools
* Install Docker on Linux: `sudo apt-get install docker`
* Install Docker-Compose on Linux: `sudo apt-get install docker-compose`


## Docker-Compose Commands
* Navigate to `./docker_compose/development` for all of the following Docker related scripts:
* Start the Docker images silently: `sudo docker-compose up -d`
* Start the Docker images actively: `sudo docker-compose up`
* Stop the Docker images: `sudo docker-compose down`
* Populate the development postgresql container with tables and data from `/postgres/database_schema.sql` and `/postgres/test_data.sql` by running `./populateDatabase.sh` (this is currently only available for development)


## PostgreSQL Login
* Port: 5432
* User: postgres
* Password: changeme


## PostgreSQL Container Commands
* access container: `docker exec -it postgres psql -U postgres`
* display tables: `\d`
* connect to db: `\c <database name> `


## PgAdmin
* Access: http://localhost:5050
* Email: pgadmin4@pgadmin.org
* Password: admin

## Backend Production Environment Guide

### Tools

* Git
* NodeJS
* NPM
* Docker
* PostgreSQL
* Gitlab-CI


### Installing the Tools

#### Ubuntu (recommended due to better Docker compatibility)

[Download the latest version](https://ubuntu.com/download/desktop)


##### Git

On ubuntu you can do this all through a terminal:

```
sudo apt update
sudo apt install git
```

After installation it can be checked in the command line with:

```
git -v
```


##### NodeJS & NPM

On ubuntu extensive instructions can be found [here](https://github.com/nodesource/distributions/blob/master/README.md#snap).

For a simplified instructions, we are going to install the snap package manager in order to access this package.

```
sudo apt update
sudo apt install snapd
```

This may take a bit.

After this is done you will be able to install node and npm:

```
sudo snap install node --classic --channel=12
```

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```

##### Node Packages

Open the backend folder. In a terminal, this can be done by being in the folder above and entering `cd ./backend/`.

To install everything else you will need to run the command `npm install`.

##### Docker and PostgreSQL

Run `sudo apt-get install docker` and `sudo apt-get install docker-compose` to install docker and docker-compose.

From your project directory, navigate to the production using either `cd docker_compose/production`.

With Docker running, run the command `docker-compose up`. This will both install PostgreSQL and start it running.

Some additional useful Docker commands can be found at the bottom of this file.


##### Starting the project

Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start`.



#### Windows

##### Git

Git is the tool used for version control for this project. Git can be installed on windows by simply downloading and running the executable from [here](https://git-scm.com/download/win).

After installation it can be checked in the command line with:

```
git -v
```

##### NodeJS & NPM

NodeJS and NPM come with each other. On Windows this is a simple install. Simply download and run the stable version installer of NodeJS from [here](https://nodejs.org/en/). The stable one always has an even number before the first dot.

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```

##### Node Packages

Open the backend folder. In a terminal, this can be done by being in the folder above and entering `cd ./backend/`.

To install everything else you will need to run the command `npm install`.

##### Docker and PostgreSQL

Download and install [Docker Desktop](https://docs.docker.com/docker-for-windows/install/) for Windows.

From your project directory, navigate to the production or development environment using either `cd docker_compose/production` or `cd docker_compose/development`.

With Docker running, run the command `docker-compose up`. This will both install PostgreSQL and start it running.

Some additional useful Docker commands can be found at the bottom of this file.


##### Starting the project

Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm run dev` to start the dev environment (automatically reloads the project when source code is changed).


## Available Scripts

Unless otherwise noted, these scripts can be run in the project directory:

## NPM tools
* Developer Start (automatically reloads the project when changes are made): `npm run dev`
* Launch the test runner: `npm test`
* Standardize code: `npm run standardize`


## Useful tools
* Install Docker on Linux: `sudo apt-get install docker`
* Install Docker-Compose on Linux: `sudo apt-get install docker-compose`


## Docker-Compose Commands
* Navigate to `./docker_compose/production` for all of the following Docker related scripts:
* Start the Docker images silently: `sudo docker-compose up -d`
* Start the Docker images actively: `sudo docker-compose up`
* Stop the Docker images: `sudo docker-compose down`
* Populate the development postgresql container with tables and data from `/postgres/database_schema.sql` and `/postgres/test_data.sql` by running `./populateDatabase.sh` (this is currently only available for development)


## PostgreSQL Login
* Port: 5432
* User: postgres
* Password: changeme


## PostgreSQL Container Commands
* access container: `docker exec -it postgres psql -U postgres`
* display tables: `\d`
* connect to db: `\c <database name> `


## PgAdmin
* Access: http://localhost:5050
* Email: pgadmin4@pgadmin.org
* Password: admin
