DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS jobs;
DROP TABLE IF EXISTS associations;
DROP TABLE IF EXISTS instructions;
DROP TABLE IF EXISTS data;
DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS assignments;

CREATE TABLE IF NOT EXISTS jobs(
    advanced_instructions VARCHAR(255),
    operation VARCHAR(255),
    part_name VARCHAR(255),
    customer VARCHAR(255),
    part_number INT,
    job_number VARCHAR(255),
    PRIMARY KEY(job_number, part_number)
);

CREATE TABLE IF NOT EXISTS users(
    password VARCHAR(255),
    permission INT,
    id VARCHAR(255),
    name VARCHAR(255),
    username VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS data(
    operator VARCHAR(255),
    machine VARCHAR(255),
    remark VARCHAR(255),
    work_order INT,
    characteristic VARCHAR(255),
    result TEXT,
    date TIMESTAMPTZ,
    shift INT,
    inspection_number INT,
    job_number VARCHAR(255),
    inspection_type VARCHAR(255),
    PRIMARY KEY(job_number,work_order, inspection_number, date)
);

CREATE TABLE IF NOT EXISTS instructions(
    inspection_number INT,
    frequency VARCHAR,
    setup_tech VARCHAR(255),
    method VARCHAR(255),
    expected VARCHAR(255),
    tolerance VARCHAR(255),
    name VARCHAR(255),
    form_type VARCHAR(255),
    job_number VARCHAR(255),
    inspection_type VARCHAR(255),
    PRIMARY KEY(job_number, inspection_number)
);

CREATE TABLE IF NOT EXISTS associations(
    id SERIAL,
    job_number VARCHAR(255),
    work_order INT,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS files(
    job_number VARCHAR(255),
    file_path VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS assignments(
    id SERIAL,
    job_number VARCHAR(255),
    inspection_number INT,
    user_id VARCHAR(255),
    PRIMARY KEY(id)
);
