-- User Table Data
INSERT INTO users (password, permission, id, name, username)
VALUES ('TestPassword123', -1, '0', 'Joe', 'MyNameIsJoe');
 
INSERT INTO users (password, permission, id, name, username)
VALUES ('admin', 3, '-3', 'Adam', 'admin');
 
INSERT INTO users (password, permission, id, name, username)
VALUES ('operator', 1, '-1', 'Odin', 'operator');
 
INSERT INTO users (password, permission, id, name, username)
VALUES ('inspector', 2, '-2', 'Ivy', 'inspector');

INSERT INTO users (password, permission, id, name, username)
VALUES ('Test Operator #2', 2, '-4', 'Ivy', 'Test Operator #2');

-- Jobs Table Data
INSERT INTO jobs (advanced_instructions, operation, part_name, customer, part_number, job_number)
VALUES ('Advanced Instructions Test', 'Operation Test', 'Part Name Test', 'Customer Test', -1, -100);

INSERT INTO jobs (advanced_instructions, operation, part_name, customer, part_number, job_number)
VALUES ('Number Value Test', 'OP test', 'Part Name', 'Customer', -2, -200);

-- Associations Table Data
INSERT INTO associations (id, job_number, work_order) 
VALUES (-1, -100, -1);

INSERT INTO associations (id, job_number, work_order) 
VALUES (-2, -200 -2);

-- Assignments Table Data
INSERT INTO assignments (id, job_number, inspection_number, user_id)
VALUES (-1, -100, 1, '-4');

INSERT INTO assignments (id, job_number, inspection_number, user_id)
VALUES (-2, -100, 2, '-2');

-- Instructions Table Data
INSERT INTO instructions (inspection_number, frequency, setup_tech, method, expected, tolerance, name, form_type, job_number, inspection_type)
VALUES (1, 'Once per Test', 'Test Inspector Man', 'Look at this data again', 'There is a checkbox here', 'zero', 'Test Instruction For Checkbox', 'Checkbox', -100, 'Operator');

INSERT INTO instructions (inspection_number, frequency, setup_tech, method, expected, tolerance, name, form_type, job_number, inspection_type)
VALUES (2, 'Once per Test', 'Test Inspector Man', 'Look at this data again', 'There is a checkbox here', 'zero', 'Test Instruction For Checkbox', 'Checkbox', -100, 'Operator');

INSERT INTO instructions (inspection_number, frequency, setup_tech, method, expected, tolerance, name, form_type, job_number, inspection_type)
VALUES (1, 'Once per Test', 'Test Inspector Man', 'Look at this data again', 'There is a checkbox here', 'zero', 'Test Instruction For Checkbox', 'Checkbox', -100, 'Operator');

INSERT INTO instructions (inspection_number, frequency, setup_tech, method, expected, tolerance, name, form_type, job_number, inspection_type)
VALUES (1, 'Once per day', 'Test Inspector Woman', 'Look at this graph!', 42, 0, 'Input Test', 'Input', -200, 'Operator');

INSERT INTO instructions (inspection_number, frequency, setup_tech, method, expected, tolerance, name, form_type, job_number, inspection_type)
VALUES (2, 'Once per score', 'Test Inspector Person', 'Look at this photograph!', 69, 420, 'Input Test 2', 'Input', -200, 'Inspector');

  
-- Data 
INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number, inspection_type)
VALUES ('-4', 'Test Machine', 'This is a test yet again', -1, 'Test Characteristic #2', 1, current_timestamp, -1, 1, -100, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', 1, 'Test Characteristic #2', 5,   current_timestamp, -1, 1, -100, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -1, 'Test Characteristic #2', 7,   current_timestamp, -1, 1, -100, 'Operator');
  


INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -1, 'Test Characteristic #2', 3,   current_timestamp, -1, 2, -100, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-2', 'Test Machine', 'This is a test yet again', 1, 'Test Characteristic #2', 2,   current_timestamp, -1, 2, -100, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-3', 'Test Machine', 'This is a test yet again', -1, 'Test Characteristic #2', 1,   current_timestamp, -1, 2, -100, 'Operator');
  
  
    
INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-1', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 7,   current_timestamp, -1, 1, -200, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 9,   current_timestamp, -1, 1, -200, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 0,   current_timestamp, -1, 1, -200, 'Operator');
  
  
  
INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-1', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 66,   current_timestamp, -1, 2, -200, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 666,   current_timestamp, -1, 2, -200, 'Operator');

INSERT INTO data(operator, machine, remark, work_order, characteristic, result, date, shift, inspection_number, job_number,  inspection_type)
Values ('-4', 'Test Machine', 'This is a test yet again', -2, 'Test Characteristic #2', 127,   current_timestamp, -1, 2, -200, 'Operator');
  
