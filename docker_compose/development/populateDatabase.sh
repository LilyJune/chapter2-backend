#!/bin/bash

sudo docker exec postgres-dev psql -U postgres -d postgres -a -f /var/lib/postgresql/data/pgdata/database_schema.sql;

sudo docker exec postgres-dev psql -U postgres -d postgres -a -f /var/lib/postgresql/data/pgdata/test_data.sql;